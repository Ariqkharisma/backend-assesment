import uuid
from .database import Base
from sqlalchemy import TIMESTAMP, Column, ForeignKey, String, Boolean, Integer, text
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import relationship

class Nasabah(Base):
    __tablename__ = 'nasabah'
    id = Column(UUID(as_uuid=True), primary_key=True, nullable=False, default=uuid.uuid4)
    nama = Column(String, nullable=False)
    nik = Column(String, unique=True, nullable=False)
    no_hp = Column(String, unique=True, nullable=False)
    no_rekening = Column(String, unique=True)
    created_at = Column(TIMESTAMP(timezone=True), nullable=False, server_default=text("now()"))
    updated_at = Column(TIMESTAMP(timezone=True), nullable=False, server_default=text("now()"))

class Rekening(Base):
    __tablename__ = 'rekening'
    id = Column(UUID(as_uuid = True), primary_key=True, nullable=False, default=uuid.uuid4)
    no_rekening = Column(String, unique=True, nullable=False)
    saldo = Column(Integer, nullable=False)
    created_at = Column(TIMESTAMP(timezone=True), nullable=False, server_default=text("now()"))
    updated_at = Column(TIMESTAMP(timezone=True), nullable=False, server_default=text("now()"))

class Transaksi(Base):
    __tablename__ = 'transaksi'
    id = Column(UUID(as_uuid=True), primary_key=True, nullable=False, default=uuid.uuid4)
    no_rekening = Column(String, nullable=False)
    kode_transaksi = Column(String, nullable=False)
    nominal = Column(Integer, nullable=False)
    created_at = Column(TIMESTAMP(timezone=True), nullable=False, server_default=text("now()"))
    updated_at = Column(TIMESTAMP(timezone=True), nullable=False, server_default=text("now()"))
