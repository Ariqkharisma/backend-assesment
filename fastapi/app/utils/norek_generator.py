import random
import math

def generate_nomor_rekening():
    no_rekening = ''

    for i in range(8):
        random_number = math.floor(random.random() * 10)
        if random_number == 10:
            random_number -= 1
        no_rekening += str(random_number)

    return no_rekening