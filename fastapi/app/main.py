from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from app.config import settings
from app.routers import nasabah, rekening, transaksi
from .database import Base, engine

app = FastAPI()

origins = [
    settings.CLIENT_ORIGIN,
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

Base.metadata.create_all(engine)
print("Creating database ....")

app.include_router(nasabah.router, tags=['Nasabah'], prefix='/api')
app.include_router(rekening.router, tags=['Rekening'], prefix='/api')
app.include_router(transaksi.router, tags=['Transaksi'], prefix='/api')