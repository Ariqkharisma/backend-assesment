from fastapi import APIRouter, status, Request, Response, Depends, HTTPException
from .. import models
from sqlalchemy import or_
from sqlalchemy.orm import Session
from ..database import get_db
from ..config import settings

router = APIRouter()

@router.get('/saldo/{no_rekening}', status_code=status.HTTP_200_OK)
async def cek_saldo(no_rekening: str, db: Session = Depends(get_db)):
    rekening = db.query(models.Rekening).filter(models.Rekening.no_rekening == no_rekening).first()

    if rekening == None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail='Rekening not found')

    return {'message': 'success', 'saldo': rekening.saldo}

@router.get('/mutasi/{no_rekening}', status_code=status.HTTP_200_OK)
def cek_mutasi(no_rekening: str, db: Session = Depends(get_db), limit: int = 10, page: int = 1):
    rekening = db.query(models.Rekening).filter(models.Rekening.no_rekening == no_rekening).first()

    if rekening == None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail='Rekening not found')
    
    skip = (page - 1) * limit
    mutasi = db.query(models.Transaksi).filter(models.Transaksi.no_rekening == no_rekening).limit(limit).offset(skip).all()

    return {'message': 'success', 'results': len(mutasi), 'page': page, 'mutasi': mutasi}