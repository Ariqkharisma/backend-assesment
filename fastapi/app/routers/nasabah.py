from fastapi import APIRouter, status, Request, Response, Depends, HTTPException
from .. import schemas, models
from sqlalchemy import or_
from sqlalchemy.orm import Session
from ..database import get_db
from ..config import settings
from ..utils.norek_generator import generate_nomor_rekening


router = APIRouter()

@router.post('/daftar', status_code=status.HTTP_201_CREATED)
async def daftar_nasabah(payload: schemas.DaftarNasabahSchema, request: Request, db: Session = Depends(get_db)):
    nasabah_query = db.query(models.Nasabah).filter(or_(models.Nasabah.no_hp == payload.no_hp, models.Nasabah.nik == payload.nik))

    nasabah = nasabah_query.first()

    if nasabah:
       raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail='NIK atau No HP sudah dipakai')
    
    rekening = generate_nomor_rekening()

    nasabah_baru = models.Nasabah(nama= payload.nama, nik= payload.nik, no_hp= payload.no_hp, no_rekening= rekening)
    rekening_baru = models.Rekening(no_rekening=rekening, saldo=0)

    db.add(nasabah_baru)
    db.add(rekening_baru)
    db.commit()
    db.refresh(nasabah_baru)
    db.refresh(rekening_baru)

    return {'status': 'success', 'data': {'nama': nasabah_baru.nama, 'no_rekening': rekening}}