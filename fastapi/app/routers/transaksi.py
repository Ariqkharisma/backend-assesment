from fastapi import APIRouter, status, Request, Response, Depends, HTTPException
from .. import schemas, models
from sqlalchemy import or_
from sqlalchemy.orm import Session
from ..database import get_db
from ..config import settings

router = APIRouter()

@router.post('/tarik', status_code=status.HTTP_201_CREATED)
async def tarik_tunai(payload: schemas.TransaksiSchema, request: Request, db: Session = Depends(get_db)):
    rekening = db.query(models.Rekening).filter(models.Rekening.no_rekening == payload.no_rekening).first()
    
    if rekening == None:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail='Nomor rekening salah')
    
    if rekening.saldo < payload.nominal:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail='Saldo tidak cukup')
    
    transaksi_baru = models.Transaksi(no_rekening= payload.no_rekening, kode_transaksi= 'D', nominal= payload.nominal)

    rekening.saldo -= payload.nominal

    db.add(transaksi_baru)
    db.commit()
    db.refresh(transaksi_baru)

    return {'message': 'success', 'saldo': rekening.saldo}
    
@router.post('/tabung', status_code=status.HTTP_201_CREATED)
async def tabung(payload: schemas.TransaksiSchema, request: Request, db: Session = Depends(get_db)):
    rekening = db.query(models.Rekening).filter(models.Rekening.no_rekening == payload.no_rekening).first()
    
    if rekening == None:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail='Nomor rekening salah')
    
    transaksi_baru = models.Transaksi(no_rekening= payload.no_rekening, kode_transaksi= 'K', nominal= payload.nominal)

    rekening.saldo += payload.nominal

    db.add(transaksi_baru)
    db.commit()
    db.refresh(transaksi_baru)

    return {'message': 'success', 'saldo': rekening.saldo}
    
