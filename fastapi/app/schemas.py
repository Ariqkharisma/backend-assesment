from pydantic import BaseModel


class DaftarNasabahSchema(BaseModel):
    nama: str
    nik: str
    no_hp: str

    class Config:
        orm_mode = True
    
class TransaksiSchema(BaseModel):
    no_rekening: str
    nominal: int


