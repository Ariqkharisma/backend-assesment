# How To Use This project:

1. clone this project

2. move to folder fastapi `cd fastapi`

3. Install package `pip install requirements.txt`

4. create database:
    - `docker-compose up -d`

    - `docker exec -it postgres bash`

    - `psql -U postgres fastapi`

    - `create database fastapi`

5. run the app:
    - `uvicorn app.main:app --host localhost --port 8000 --reload`

6. see the documentation
    - go to `localhost:8000/docs`

